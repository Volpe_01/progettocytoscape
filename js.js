let nodi = new Array(); //array di vertici
let indice = 0; //totale n°vertici
let nArco = 0; //totale n°archi
let archi = new Array(); //array che contiene il peso degli archi
let costoPercorso = 0; //costo totale con l'algoritmo di bellmanford
window.onload = function () {
	let inputtxt = document.getElementById('testo');
	let cy = cytoscape({
		container: document.getElementById('cy'), // container to render in

		style: [// the stylesheet for the graph
			{
				selector: 'node',
				style: {
					'background-color': '#666',
					'label': 'data(id)'
				}
			},

			{
				selector: 'edge',
				style: {
					'width': 2,
					'line-color': '#ccc',
					'curve-style': 'bezier', // .. and target arrow works properly
					'label': 'data(peso)',
					//'target-arrow-color': '#ccc',
					//'target-arrow-shape': 'triangle'
				}
			}
		],
	});
	let node;
	document.getElementById("addnode").addEventListener("click", function () { //aggiunge un nodo
		peso = inputtxt.value  //il peso viene preso scelto dall'utente e scritto nell'input
		node = {
			group: 'nodes',
			data: {id: indice, peso: peso, appartenenza: indice, esaminato: false},
			position: {x: Math.random() * (500), y: Math.random() * (500)}
		};
		cy.add(node);
		nodi[indice] = node.data, //aggiunta del nodo all'array di nodi
				indice++;
	});
	 
	document.getElementById("addedgesNotOriented").addEventListener("click", function () { //aggiungi archi non orientati
		//creazione matrice per gli archi in base al numero di nodi inseriti
		let matrice = new Array(indice + 1);
		for (let i = 0; i <= indice; i++) {
			matrice[i] = new Array(indice + 1);
		}
		//riempire l'array con 0 e 1
		let p;
		for (let i = 0; i < indice; i++) {
			for (let j = 0; j < indice; j++) {
				matrice[i][j] = Math.floor(Math.random() * 2);
				if (matrice[i][j] == 1) { //se il valore casuale è 1 vuol dire che esiste l'arco
					p = Math.floor(Math.random() * 50); //quindi viene riempito con un valore casuale tra 1 e 50
					cy.add([{
							group: 'edges',
							data: {id: nArco + 'a', source: i, target: j, peso: p},
						},
					]);
					archi[nArco] = p; //aggiunto il peso all'array degli archi
					nArco++;
				}
			}
			;
		}
		;

	});
	document.getElementById("addedgesOriented").addEventListener("click", function () { //aggiungi archi orientati
		//creazione matrice per gli archi in base al numero di nodi inseriti
		let matrice = new Array(indice + 1);
		for (let i = 0; i <= indice; i++) {
			matrice[i] = new Array(indice + 1);
		}
		//riempire l'array con 0 e 1
		let p;
		for (let i = 0; i < indice; i++) {
			for (let j = 0; j < indice; j++) {
				matrice[i][j] = Math.floor(Math.random() * 2);
				if (matrice[i][j] == 1) { //se il valore casuale è 1 vuol dire che esiste l'arco
					p = Math.floor(Math.random() * 50); //quindi viene riempito con un valore casuale tra 1 e 50
					cy.add([{
							group: 'edges',
							data: {id: nArco + 'a', source: i, target: j, peso: p},
							style: {'target-arrow-color': '#ccc', 'target-arrow-shape': 'triangle'},
						}
					]);
					archi[nArco] = p; //aggiunto il peso all'array degli archi
					nArco++;
				}
			}
			;
		}
		;

	}),
			document.getElementById("Kruskal").addEventListener("click", function () {  //algoritmo bellmanford
		for (var j = 0; j < (archi.length); j++) {
			let minimo = archi[0];
			let cont = 0;
			for (var i = 0; i < archi.length; i++) {
				if (archi[i] < minimo) {
					minimo = archi[i];  //con una ricerca su ogni arco viene trovato l'arco con il peso minore
					cont = i;
				}
			}
			console.log("il minimo è " + minimo); //mostrato il valore minimo
			let arcoMin = cy.getElementById(cont + 'a'); //arco piu piccolo
			console.log("appartenenza sorgente inizialmente: " + nodi[arcoMin.data('source')].appartenenza)
			console.log("appartenenza destinazione inizialmente: " + nodi[arcoMin.data('target')].appartenenza)
			if (arcoMin.data('source') == arcoMin.data('target')) { //se l'arco è una corda, cioè sorgente e destinatario sono gli stessi
				cy.remove(arcoMin);//corda eliminata
			}
			if (nodi[arcoMin.data('source')].appartenenza !== nodi[arcoMin.data('target')].appartenenza) { //controllo se l'appartenenza è diversa 
				let aiuto = nodi[arcoMin.data('target')].appartenenza;
				for (let k = 0; k < indice; k++) {  //se è diversa entro nell'if e con un ciclo for scorro tutte le posizione dell'array di nodi
					console.log("appartenenza del nodo k " + k + " è: " + nodi[k].appartenenza)
					console.log("appartenenza nodo destinazione da cercare è: " + aiuto)
					if (nodi[k].appartenenza == aiuto) { //se l'appartenenza del nodo alla pos k è uguale all'appartenenza del nodo destinazione
						nodi[k].appartenenza = nodi[arcoMin.data('source')].appartenenza;//il nodo destinazione avrà come appartenenza quella del nodo di origine
						console.log("la nuova appartenenza del nodo k " + k + "è: " + nodi[arcoMin.data('source')].appartenenza);
					}
				}
				arcoMin.animate({
					style: {'line-color': '#ff0000'}
				}, {duration: 5000})
				costoPercorso += archi[cont];
				archi[cont] = 100 //alla posizone dell'array dell'arco appena evidenziato metto un valore grande cosi diventa ininfluente
			} else {
				archi[cont] = 100
			}
                }
		document.getElementById("output").innerHTML += "il costo minimo con l'algoritmo di Bellmanford è : " + costoPercorso + "<br/>";
		console.log("il costo minimo con l'algoritmo di Kruskal è : " + costoPercorso);
	});


	document.getElementById("Dijkstra").addEventListener("click", function () {  //algoritmo bellmanford
		let minore = 0;  //contiene il numero del vertice con il peso piu basso
		let cambiaPeso = 0;
		let arcoIniz; //arco inizale
		let minorePrec;
		for (var j = 0; j < nodi.length; j++) { // for utilizzato per scorrere tutti i nodi
			for (var i = 0; i < archi.length; i++) {
				arcoIniz = cy.getElementById(i + 'a'); //prendo l'arco nell'array di archi alla posizione i
				if (arcoIniz.data('source') == arcoIniz.data('target')) { //se l'arco è una corda, cioe sorgente e destinatario sono gli stessi
					cy.remove(arcoIniz);//corda eliminata
				} else if (arcoIniz.data('source') == minore && arcoIniz.data('target') != 0) {  //controllo se l'origine dell'arco parte da 0
					cambiaPeso = (eval(arcoIniz.data('peso')) + eval(nodi[minore].peso));//eval è una specie di puntatore che prende il valore di una variabile
					if (((nodi[arcoIniz.data('target')].peso > cambiaPeso) || (nodi[arcoIniz.data('target')].peso == Number.POSITIVE_INFINITY) || nodi[arcoIniz.data('target')].peso == 0)) {
						nodi[arcoIniz.data('target')].peso = cambiaPeso; //il vertice del destinatario dell'arco avrà come nuovo peso il peso del nodo collegato piu l'arco che li collega
						console.log("il peso del nodo " + arcoIniz.data('target') + " èeeee: " + cambiaPeso);
					}
				} else {
					if (!nodi[arcoIniz.data('target')].esaminato && nodi[arcoIniz.data('target')] != 0 && nodi[arcoIniz.data('target')] >= 50) {
						nodi[arcoIniz.data('target')].peso = Number.POSITIVE_INFINITY; //gli altri vertici avranno infinito
					}
				}
				nodi[minore].esaminato = true;
			}
			console.log("")
			console.log("esaminato il nodo: " + minore)
			if (j < nodi.length) {
				let aiuto = 0;
				while (aiuto < nodi.length && (nodi[aiuto].esaminato || nodi[aiuto].peso == 0)) {
					aiuto++;
				}
				minore = aiuto;
				if (minore < nodi.length && minorePrec != minore) {
					console.log("la variabile minore è: " + minore + ", il cui valore è: " + nodi[minore].peso);
					for (var k = 1; k < nodi.length; k++) {
						console.log("")
						console.log("nodo " + k + " ha come peso: " + nodi[k].peso + "    " + nodi[k].esaminato)
						console.log("il nodo k è minore di " + nodi[minore].peso)
						if ((nodi[k].peso < nodi[minore].peso) && (!nodi[k].esaminato) && (nodi[k].peso != Number.POSITIVE_INFINITY) && (nodi[k].peso != 0)) {  //se il peso del vertice è il più piccolo e non è stato esaminato
							console.log("entrato nell'if")
							minore = k;
						}
					}
				}
			} else {       //se minore è più grande della lunghezza dell'array dei nodi non si possono fare ulteriori modifiche
				j = nodi.length - 1;  //per cui finisce l'algoritmo uscendo dalla funzione 
			}
		}
		for (var fine = 0; fine < nodi.length; fine++) {
			document.getElementById("output").innerHTML += "il vertice " + fine + " ha come peso: " + nodi[fine].peso + "<br/>";
			console.log("il vertice " + fine + " ha come peso: " + nodi[fine].peso);
			//nodi[fine].id = nodi[fine].id + "->" + nodi[fine].peso;
		}
	});
}
