Volpe Davide 4�IB
                            PROGETTO CYTOSCAPE(Kruskal e Dijkstra)
Lo scopo di questo progetto � la realizzazione dei 2 algoritmi citati sul titolo con l�aiuto di cytoscape.
TEORIA:
-Kruskal: L'algoritmo di Kruskal�� un�algoritmo�ottimo utilizzato per calcolare gli�alberi di supporto minimi�
 di un�grafo�non orientato e con gli archi con costi non negativi.
 Prende il nome dal matematico americano�Joseph Kruskal�che lo ide� e propose nel�1956.
 Si consideri un�grafo�non orientato e connesso dove V rappresenta il numero di vertici (o nodi) ed E il numero 
 di spigoli (o archi). Ad ogni spigolo � associato un peso (o distanza): lo scopo dell'algoritmo � quello di 
 trovare un albero ricoprente di peso minimo, cio� quello in cui la somma dei pesi sia minima. L'algoritmo pu� 
 essere applicato solo se si dispone di due o pi� vertici.
 L'algoritmo di Kruskal si basa sulla seguente semplice idea: ordiniamo gli archi in ordine crescente di costo 
 e successivamente li analizziamo singolarmente, inseriamo l'arco nella soluzione se non forma cicli con gli archi
 precedentemente selezionati. Notiamo che ad ogni passo, se abbiamo pi� archi con lo stesso costo, � indifferente 
 quale viene scelto.
(fonte: wikipedia)
- Dijkstra: L'algoritmo di Dijkstra�� un�algoritmo�utilizzato per cercare i�cammini minimi�in un�grafo�con o senza
 ordinamento, ciclico e con pesi non negativi sugli archi.
(fonte: wikipedia)




CODICE:
-html: Nel codice riguardante la parte di html sono stati creati diversi div, tra cui i pi� importanti sono: uno che 
 rappresenta cytoscape, uno che rappresenta l�output finale e uno che rappresenta tutti i bottoni, i quali si dividono
 a sua volta in 3 div: uno per la creazione dei nodi, uno per l�algoritmo di kruskal e l�ultimo per l�algoritmo di dijkstra
-css:  Il codice riguardante il css viene utilizzo per migliorare la grafica.
 Vengono colorati tutti i bottoni in maniera diversa, in modo che l�utente finale li riconosca. Ai vari div viene data una 
 dimensione omogenea rispetto al resto della pagina. Come sfondo viene utilizzato una Gif che richiama il tema dei nodi.
-javaScript: Il codice riguardante la parte JS � il pi� importante in quanto � in questa parte  del progetto che vengono 
 utilizzati gli algoritmi.
 Inizialmente, dopo la creazione di alcune variabili globali, si procede con il controllo del DOM, molto importante per il funzionamento.
 Subito dopo aver creato una variabile cytoscape viene assegnato come container il div creato nell�html con id �cy� e 
 viene modificato lo style sia per i nodi che per gli archi.

 La prima funzione creata � l�addnode: quando l�utente preme sul pulsante addNode viene creato un oggetto node, con id un 
 indice che inizialmente � 0 e man mano che vengono creati nodi viene incrementato di 1, come peso si ha il valore 
 che l�utente ha messo sull�input, come appartenenza ha il suo id e su esaminato ha false(appartenenza serve per l�algoritmo di kruskal),
 mentre esaminato per l�algoritmo di dijkstra).

 Successivamente ci sono 2 metodi simili, cio� addedgesNotOriented e addedgesOriented, in cui la differenza � che un 
 addedgesNotOriented genera archi non orientati mentra l�altro crea archi orientati.
 Entrando nel dettaglio quando viene premuto uno dei 2 pulsanti viene creata una matrice con il numero di righe
 uguali al numero di colonne il cui valore � il numero di nodi creati. Per ogni posizione della matrice viene generato un 
 numero casuale tra 0 e 1. Se il valore � 1 viene generato un arco, orientato o non, in base al pulsante premuto, 
 in cui la sorgente del nodo � la posizione della riga mentre il destinatario � la colonna. Quindi il numero di archi 
 pu� variare tra 0 e (numero di vertici)^2. Il peso di ogni arco � un numero casuale tra 1 e 50.

 Dopo la creazione degli archi si procede in 2 modi, se gli archi non sono orientati viene eseguito l�algoritmo di kruskal,
 altrimenti quello di dijkstra.



Kruskal: 
Idea: quest�algoritmo viene ripetuto per tante volte quante il numero di archi totali. Ogni volta viene trovato l�arco 
 con il peso minore e vengono cancellate eventuali corde. Per evitare cicli ogni nodo inizialmente ha una sua appartenenza. 
 quando 2 nodi con appartenenza diversa vengono legato con un arco, a fine ciclo devono avere la stessa appartenenza, 
 cio� quella del nodo sorgente, e cosi tutti i nodi che avevano l�appartenenza del nodo di destinazione avranno 
 l�appartenenza del nodo sorgente. Se vengono collegati 2 nodi con la stessa appartenenza si formerebbe un ciclo. 
 Alla fine gli archi esaminati vengono colorati di rosso e sull�output viene scritto il costo minimo per l�algoritmo.

Procedimento:viene eseguito un ciclo for con variabile j=0 fino al numero di archi totali.
 Con un for viene cercato e salvato l�arco con il valore del peso pi� piccolo.
 Subito dopo viene controllato se � una corda, se lo � l�arco viene eliminato altrimenti viene controllato se l�appartenenza 
 dell�arco di origine sia diversa dall�appartenenza dell�arco di destinazione. Se fosse la stessa appartenenza 
 si formerebbe un ciclo per cui l�algoritmo fallirebbe.
 Arrivati a questo punto l�idea � che il nodo di destinazione assuma l�appartenenza del nodo di origine cosi 
 come tutti i nodi che hanno come appartenenza il valore del nodo di destinazione.
 � una tecnica molto efficace per evitare cicli.
 Superati questi controlli, l�arco interessato viene colorato di rosso e il suo peso viene posto a 100, cosi quando si 
effettueranno i controlli per l�arco con il peso pi� basso, quelli esaminati non intralciano.




Dijkstra:
Idea: l�algoritmo inizia dal nodo 0. Vengono esaminati tutti gli archi nella quale il nodo di origine � lo 0 e il nodo 
 a cui � collegato quest�arco avr� come peso il peso del nodo 0 pi� il peso dell�arco che collega i 2 nodi. 
 Gli altri nodi che non sono collegati al nodo 0 avranno come peso infinito.
 Ora il vertice 0 � esaminato per cui viene controllato quale nodi tra quelli non orientati e con peso diverso da infinito 
 abbia il peso pi� piccolo. Una volta trovato viene svolto lo stesso procedimento svolto dal nodo 0. 
 L�algoritmo finisce quando tutti i nodi sono esaminati.

Procedimento: viene eseguito un for con j=0 e continua finche j non supera il valore dei nodi creati.
 Con un altro for con i=0 e continua finche i non supera il numero di archi viene controllato se l�arco selezionato non sia una corda,
 se lo � viene eliminato. Subito dopo viene controllato se il nodo di origine dell�arco � quello con il peso pi� basso 
 e se il destinatario non sia lo 0. Se non lo �, e il nodo di destinatario non � esaminato, il nodo del destinatario avr� come peso infinito. 
 se invece se il nodo di origine dell�arco ha il peso pi� basso viene controllato se il valore che potrebbe assumere sia pi� piccolo
 del valore del peso che ha gi�. Se � pi� piccolo viene cambiato altrimenti rimane invariato. 
 a fine ciclo il nodo sar� esaminato e non potr� pi� essere cambiato 



                           
